/*
*       The programm developed for studing C language

*       (c) 2018 The programm is developed by dundel.
*/

#include <stdio.h>
#include <stdlib.h>


int main()
{
    int c;

    while((c = getchar()) != EOF)
    {
           putchar(c);
    }
    return 0;
}
